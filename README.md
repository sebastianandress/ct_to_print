# Disclaimer

Although we tested this algorithm on 26 acetabular fractures and used it for surgeries on 14 patients which all were successful, we can't ensure that the result of the algorithm fits perfect the anatomy of the patient. **Right now it is not a medical product. Use at your own risk.** We won't assume liability for this product.


# Research Study

We are doing a research study with this product. Please participate if you want to try it with an own person. Feel free to write any questions at [simon.weidert@med.uni-muenchen.de](mailto:simon.weidert@med.uni-muenchen.de) or [sebastian.andress@med.uni-muenchen.de](mailto:sebastian.andress@med.uni-muenchen.de).


# Workflow to create a 3D printable model out of a CT-Volume of an Acetabular fracture

## Slicer

Slicer is an open source Software. You can download it from this [link](http://download.slicer.org). Please download the latest stable version for your operation system and install it.

Please follow the [Video](https://1drv.ms/v/s!AqzdGuIdWLfehPt5QPNtt9w8GLSoHg). All Steps are also described in the following.

### 1. Loading the Volume

* Load your DICOM-Data into slicer by pressing the **DICOM-Button** to open the DICOM-Browser. By pressing "**Import**" you can also import new directories.

### 2. View and render the Volume

Modules: **Volumes** and **Volume Rendering**

* Select your preferred layout by clicking the **Layout selector**.

* To set the window level of the CT search for the "**Volumes**"-Module and select a fitting level.

* The cropping of the next step can be simplified to render the existing volume. This is done by the **Volume Rendering**-Module. First display the Model on the 3D-View by clicking the **Eye-Icon**. If the rendering won't appear, try clicking on the **center button** at the top of the 3D-View. Adjust the shift by moving the "**Shift**"-Slider. You can also change the presets to enhance the rendering.

### 3. Crop the volume

Module: **Crop Volume**

* Crop the Volume by using the **Crop Volume**-Module. By pressing "**Display ROI**" you can activate the markers. Finish the adjustment by pressing "**Apply**".

### 4. Segmentation of the volume and remove unnecessary parts

Module: **Editor**

* Click the **Threshold-Effect**-Button and select a appropriate **HU-Range**.
* Click the **Paint-Effect**-Button and painting using **Background Color** to remove the femoral head. Make sure to use **Sphere** painting and set the sphere **radius** as suitable.
* Check the result by clicking the **Save Islands-Effect**-Button and selecting the Pelvis.
* Remove other dispensable parts and use the **Save Islands-Effect** again.
* Segment the model by using the **Create Model-Effect**. Make sure to select the desired **Label Color** and click "**Apply**".


### 5. Save the model

* The created model can be exported by choosing File -> Save or by pressing the **Save**-Icon. Select the model from segment 1, choose "**.stl**" as File Format and a Directory and hit "**Save**"


## Blender

Blender is also a very powerful open source software. It can edit meshes as well as creating animations. Although it comes with many Filters. Please download this software at this [link](https://www.blender.org/download/).

Please follow the [Video](https://1drv.ms/v/s!AqzdGuIdWLfehPt73cyX7S3oCJ6RLQ). All Steps are also described in the following.

### Setup Blender

* Go to "**File**" -> "**User Preferences...**" and open the "**Input**"-Tab. If you don't have a numpad, check "**Emulate Numpad**". Also if you don't have a 3rd mouse button, check "**Emulate 3 Button Mouse**" The second one is important to navigate in the Scene, because you do this by clicking the middle mouse button. With the emulation activated you can use "Alt" + left Mouse instead.

* To import the Add-on, open the "**Add-ons**"-Tab. Click on "**Install from File...**" and select the Python file, which you can download [here](addon/ct_to_print.py). Press again "**Install from File...**". Now activate the Add-on by checking the box. Also search for the **Import-Export: STL-format** Add-on and activate it.

* Clear the scene by clicking on the three objects (standard: right click!) and deleting them by pressing "**x**" on the keyboard twice. If you are happy with the setup, select "**File**" -> "**Save Startup File**" (you have to double-click).

### Recreate a hollow mesh

* The installed plugin can be found in the "**Medicine**"-Tab at the left side.

* Click on "**Import STL**" and select the file, which you created with Slicer before. Then import the file by again clicking on "**Import STL**".

* Center the mesh by clicking on "**Set Origin and Define**".

* Before you recreate the mesh, check the settings. Especially the "**Corticalis Thickness**" and "**Fracture Detail**" settings are important. Feel free to try the other settings too. A description will appear as you place the mouse above it. Then press "**Recreate printable Bone**".

* The recreation process will take about one minute depending on the used computer hardware.

* Export the mesh by clicking on "**Export STL**". It is important to check "**Selection Only**" at the left bottom. Then press  "**Export STL**".


## Set up the print with the software of your 3D printer

The created mesh is watertight. Set up the printer with the software of your choice and print the mesh.