bl_info = {
    "name" : "CT to Print",
    "author" : "Sebastian Andreß",
    "version" : (1.0),
    "blender" : (2, 77, 0),
    "location" : "View3D > Tools",
    "description" : "Optimize a segmented mesh for 3D Printing. Not a medical product. Use at your own risk.",
    "category" : "Object"}


import bpy
import bmesh
import time
import mathutils
import math
from bpy.props import *


### Properties ###

# create Spongiosa
bpy.types.Scene.createGrid = BoolProperty(
    name="Create Spongiosa-Grid",
        description="If checked, please notice the Spongiosa-Settings.",
        default = False)
bpy.types.Scene.distancePillarX = FloatProperty(
    name = "Pillar Distance X", 
    description = "For pillars in x-Direction",
    default = 5,
    min = 0.01)
bpy.types.Scene.distancePillarY = FloatProperty(
    name = "Pillar Distance Y", 
    description = "For pillars in y-Direction",
    default = 5,
    min = 0.01)
bpy.types.Scene.distancePillarZ = FloatProperty(
    name = "Pillar Distance Z", 
    description = "For pillars in z-Direction",
    default = 5,
    min = 0.01)
bpy.types.Scene.diameterPillarX = FloatProperty(
    name = "Pillar Diameter X", 
    description = "For pillars in x-Direction",
    default = 1,
    min = 0.01)
bpy.types.Scene.diameterPillarY = FloatProperty(
    name = "Pillar Diameter Y", 
    description = "For pillars in y-Direction",
    default = 1,
    min = 0.01)
bpy.types.Scene.diameterPillarZ = FloatProperty(
    name = "Pillar Diameter Z", 
    description = "For pillars in z-Direction",
    default = 1,
    min = 0.01)    


# clarify Fracture    
bpy.types.Scene.clarifyFracture = BoolProperty(
    name="Clarify Fracture Gap",
        description="If checked, please notice the Fracture-Gap-Settings.",
        default = False)

bpy.types.Scene.radiusGaps = FloatProperty(
    name = "Precision", 
    description = "The rad-Angle between two gaps and a bridging Fragment. At 3.2 no face is going to be deleted, at 0 everything",
    default = 2.8,
    min = 0.1,
    max = 3.1416)

bpy.types.Scene.distanceGaps = FloatProperty(
    name = "Max. distance between Gaps", 
    description = "The maximum distance between two gaps and a bridging Fragment. At 0 no face is going to be deleted, at infinite everything",
    default = 3,
    min = 0.1)

# solidify        
bpy.types.Scene.thicknessCorticalis = FloatProperty(
    name = "Corticalis Thickness", 
    description = "The thickness of the recreated mesh.",
    default = 0.8,
    min = 0.01,
    max = 10)

bpy.types.Scene.fractureDetail = FloatProperty(
    name = "Fracture Detail", 
    description = "Higher number will erase holes, but also small fracture gaps.",
    default = 0.4,
    min = 0.01,
    max = 5)

# projection/shrinkwrap
bpy.types.Scene.shrinkwrap_first_offset = FloatProperty(
    name = "Offset first Shrinkwrap-Loop", 
    description = "Choose a greater offset if object has very much sharp edges.",
    default = 8,
    min = 0,
    max = 50)

bpy.types.Scene.projection_offset = FloatProperty(
    name = "Offset Projection", 
    description = "Choose a greater offset if object has very much sharp edges. Has to be greater than 'Offset first Shrinkwrap-Loop'!",
    default = 0,
    min = 0,
    max = 50)
    
bpy.types.Scene.projection_poke_repeats = FloatProperty(
    name = "Number of poke-repeats", 
    description = "The higer, large faces will be subdivided more often.",
    default = 3,
    min = 0,
    max = 10)
    
bpy.types.Scene.projection_max_length_edge = FloatProperty(
    name = "Max. length of edges", 
    description = "When algorithm searches the large faces, searches for the length of its edge. If an edge is larger then this value, the edge will be subdivided.",
    default = 15,
    min = 0,
    max = 200)

bpy.types.Scene.projection_max_distance_verts = FloatProperty(
    name = "Max. distance verts", 
    description = "If the original mesh has small fracture gaps for example in the acetabular joint, the number should be small.",
    default = 50,
    min = 0,
    max = 200)

bpy.types.Scene.projection_min_length = FloatProperty(
    name = "Min. length of projection", 
    description = "Enlarge just in specific cases.",
    default = 0,
    min = 0,
    max = 1000)

bpy.types.Scene.projection_max_length = FloatProperty(
    name = "Max. length of projection", 
    description = "If for example the acetabular joint is very deep, enlarge this number.",
    default = 40,
    min = 0,
    max = 1000)


    

### Helper Functions ###

def joinObjects(obs):
    '''
    This class joins the meshs
    Args:
        obs: list of Meshs.
    '''
    
    scene = bpy.context.scene
    bpy.ops.object.select_all( action = 'DESELECT' )
    for o in obs:
        o.select = True
    scene.objects.active = obs[0]
    bpy.ops.object.join()   

def booleanMod(operation, objA, objB):
    '''
    Applies the Boolean Modifier
    Args:
        operation (int): 0 for DIFFERENCE, 1 for UNION, 2 for INTERSECT
        objA: mesh object on which modifier will be applied.
        objB: mesh object to use for modifier.
    '''
    scene = bpy.context.scene
    boMoUn = objA.modifiers.new('boUn', 'BOOLEAN')
    boMoUn.object = objB
        
    if operation == 0:
        boMoUn.operation = 'DIFFERENCE'
    if operation == 1:
        boMoUn.operation = 'UNION'
    if operation == 2:
        boMoUn.operation = 'INTERSECT'
        
    scene.objects.active = objA
    bpy.ops.object.modifier_apply(apply_as='DATA', modifier='boUn')


### Interface ###

class sizeVar(bpy.types.Panel):
    bl_space_type="VIEW_3D"
    bl_region_type="TOOLS"
    bl_label="CT to Print"
    bl_category="Medicine"
    
    def draw(self, context):
        '''Creates the Interface'''
        layout = self.layout
        sce = context.scene
        ## Settings
        
        col = layout.column(align=True)
        col.label ('Settings:')
        col.row().prop(sce, 'thicknessCorticalis')
        col.row().prop(sce, 'fractureDetail')
        
        col = layout.column(align=True) 
        
        col.row().prop(sce, 'createGrid')
        col.row().prop(sce, 'distancePillarX')
        col.row().prop(sce, 'distancePillarY')
        col.row().prop(sce, 'distancePillarZ')
        col.row().prop(sce, 'diameterPillarX')
        col.row().prop(sce, 'diameterPillarY')
        col.row().prop(sce, 'diameterPillarZ')
        
        col = layout.column(align=True) 
        
        col.row().prop(sce, 'clarifyFracture')
        col.row().prop(sce, 'radiusGaps')
        col.row().prop(sce, 'distanceGaps')
                
        col = layout.column(align=True)
        col = layout.column(align=True)
        
        ## Advanced Settings
        
        col.label('Advanced Settings:')
        col.row()
        col.row().prop(sce, 'shrinkwrap_first_offset')
        col.row().prop(sce, 'shrinkwrap_first_offset')
        col.row().prop(sce, 'projection_offset')
        col.row().prop(sce, 'projection_poke_repeats')
        col.row().prop(sce, 'projection_max_length_edge')
        col.row().prop(sce, 'projection_max_distance_verts')
        col.row().prop(sce, 'projection_min_length')
        col.row().prop(sce, 'projection_max_length')
        
        col = layout.column(align=True)
        col = layout.column(align=True)
        
        ## Tool
        
        col.label('Tool:')
        col.row()
        col.operator("import_mesh.stl")
        col.operator("ct.originjoin")
        col.row()
        
        col = layout.column(align=True) 
        col.operator("object.mode_set", text= 'Set Edit Mode').mode='EDIT'
        col.operator("mesh.select_all").action='TOGGLE'
        col.operator("mesh.select_linked")
        col.operator("view3d.select_border")
        col.operator('ct.invertdelete')
        col.operator("object.mode_set").mode='OBJECT'
        
        col = layout.column(align=True) 
        
        col.operator("object.transform_apply").rotation=True
        
        col = layout.column(align=True) 
        
        col.operator("ct.recreatect")

        col = layout.column(align=True) 
        col.operator("export_mesh.stl")
        
        
        

### Custom Operators ###

class RecreateCTOperator(bpy.types.Operator):
    '''
    Runs after clicking "Recreate printable Bone".
    '''
    
    bl_idname = "ct.recreatect"
    bl_label = "Recreate printable Bone"

    def execute(self, context):
        
        scene = bpy.context.scene
        
        bpy.ops.object.mode_set(mode='OBJECT')
        
        ## Join & Define ##
        ''' Joins all mesh objects in the scene and defines the result as "objCT"'''
        
        time_start = time.time()
        
        for ob in scene.objects:
            ob.hide = False
        if len(scene.objects) == 0:
            #TODO: Error Message
            print('no object')
            self.report({'ERROR'}, "No Object found. Please import or create a Object.")
            
        if len(scene.objects) == 1:
            objCT = scene.objects[0]
        else:
            objCT = joinObjects(scene.objects)
        
        objCT = scene.objects.active
        objCT.name = 'Bone'
        objCT.select = True
        
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS')
        objCT.location = (0,0,0)
        bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)
        
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        print("Objects joined")


        ## Shrinkwrap ##
        '''
        1. Creates a sphere which is called "objRec"
        2. Applies 6 times a shrinkwrap modifier on it with the "objCT" as target and then the remesh modifier it afterwards. The remesh won't be done in the last loop.
        3. Searches for large faces
        4. Devides these large faces and ray casts them to "objCT".
        5. Applies 7 times a shrinkwrap modifier on it with the "objCT" as target and then the remesh modifier it afterwards. The remesh will be done in the last loop.
        '''
        
        offset_shrinkwrap = scene.shrinkwrap_first_offset
        offset_projection = scene.projection_offset
        poke_repeats = scene.projection_poke_repeats
        max_length_edge = scene.projection_max_length_edge
        distance_verts = scene.projection_max_distance_verts
        min_length_projection = scene.projection_min_length
        max_length_projection = scene.projection_max_length
        
        
        # create Sphere #
        
        objRec = bpy.ops.mesh.primitive_ico_sphere_add(subdivisions = 5, size=sum(objCT.dimensions), location=objCT.location)
        objRec = bpy.context.active_object
        objRec.name = 'Bone_Recreation'
        
        # first shrinkwrap-loop #

        i = 0
        while i < 6:

            mod_shrink = objRec.modifiers.new('mosh', 'SHRINKWRAP')
            mod_shrink.target = objCT
            mod_shrink.offset = offset_shrinkwrap
            mod_shrink.use_keep_above_surface = True
            mod_shrink.wrap_method = 'NEAREST_SURFACEPOINT'

            bpy.ops.object.modifier_apply(apply_as='DATA', modifier='mosh')

            if i < (6 - 1):
                objRec = bpy.context.active_object
                mod_remesh = objRec.modifiers.new('more', 'REMESH')
                mod_remesh.mode = 'SMOOTH'
                mod_remesh.use_remove_disconnected = False
                mod_remesh.octree_depth = 6
                bpy.ops.object.modifier_apply(apply_as='DATA', modifier='more')

            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
            
            i = i + 1
        
        
        # find great faces #
        
        bpy.ops.object.mode_set(mode='EDIT')
        bpy.ops.mesh.normals_make_consistent(inside=False)
        mesh_obj = bpy.context.edit_object
        me = mesh_obj.data
        bm = bmesh.from_edit_mesh(me)
        
        long_edge_list = list()
        for edge in bm.edges:
            if edge.calc_length() > max_length_edge:
                long_edge_list.append(edge)
                
        
        great_face_list = list()
        for face in bm.faces:
            face.select = False
            edge_i = 0
            for face_edge in face.edges:
                for long_edge in long_edge_list:
                    if face_edge == long_edge:
                        edge_i = edge_i + 1
            if edge_i > 2:
                edge_sum = 0
                for face_edge in face.edges:
                    edge_sum = edge_sum + face_edge.calc_length()
                if edge_sum > (max_length_edge * 2):
                    face.select = True
        
        #Hackaround because bmesh.ops not working
        #poke_face_list = bmesh.ops.poke(bm, faces=bm.faces).faces
        
        
        i = 0
        while i < poke_repeats:
            bpy.ops.mesh.poke()
            i=i+1
        
        # project #
        
        coordinate_dict = {}
        
        me.update
        bmesh.update_edit_mesh(me)
        
        if hasattr(bm.verts, "ensure_lookup_table"): 
            bm.verts.ensure_lookup_table()
        
        poke_faces = [f for f in bm.faces if f.select]
        poke_verts = [v for v in bm.verts if v.select]
        
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        
        vert_location_dict = {}
        
        for face in bm.faces:
            if face.select == True:
                for vert in face.verts:
                    origin = vert.co
                    direction = face.normal *(-1)
                    
                    (res ,location, normal, index) = objCT.ray_cast(origin, direction)
                    loc_new = location - (face.normal * offset_projection)
                    length = (-vert.co + location).length
                    
                    vert_location_dict.update({vert.index:(res,loc_new,length)})
                 
        
        for vert in bm.verts:
            if vert.select == True:
                ## check result
                if vert_location_dict[vert.index][0] == True:
                    
                    ## check min length
                    if vert_location_dict[vert.index][2] > min_length_projection:
                        
                        if vert_location_dict[vert.index][2] < max_length_projection:
                        
                            ## check distance between two new locations with positive result
                            if vert.index != 0:
                                
                                # search last res == True
                                x = 1
                                index = vert.index
                                while (vert.index - x) > 0:
                                    if (vert.index - x) in vert_location_dict:
                                        index = vert.index - x
                                        break
                                    else:
                                        x = x+1
                                
                                distance = (-vert_location_dict[vert.index][1] + vert_location_dict[index][1]).length
                        
                                if distance < distance_verts:
                                
                                    if vert.index != (len(bm.verts) - 1):
                                
                                        # search next res == True
                                        x = 1
                                        index = vert.index
                                        while (vert.index + x) < (len(bm.verts) - 1):
                                            if (vert.index + x) in vert_location_dict:
                                                index = vert.index + x
                                                break
                                            else:
                                                x = x+1
                                    
                                        distance = (-vert_location_dict[vert.index][1] + vert_location_dict[index][1]).length
                                
                                        if distance < distance_verts:
                                            vert.co = vert_location_dict[vert.index][1]
                                            vert.select = False

        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        
        me.update
        bmesh.update_edit_mesh(me)
        
        bpy.ops.object.mode_set(mode='OBJECT')
        
        # second shrinkwrap-loop #
        
        objRec = bpy.context.active_object
        mod_remesh = objRec.modifiers.new('more', 'REMESH')
        mod_remesh.mode = 'SMOOTH'
        mod_remesh.use_remove_disconnected = False
        mod_remesh.octree_depth = 8
        bpy.ops.object.modifier_apply(apply_as='DATA', modifier='more')
        
        iterations=7
        count=0
        while (count < iterations):
            mod_shrink = objRec.modifiers.new('mosh', 'SHRINKWRAP')
            mod_shrink.target = objCT
            mod_shrink.offset = 0
            mod_shrink.use_keep_above_surface = False
            mod_shrink.wrap_method = 'NEAREST_SURFACEPOINT'
            
            mod_remesh = objRec.modifiers.new('more', 'REMESH')
            mod_remesh.mode = 'SMOOTH'
            mod_remesh.use_remove_disconnected = False
            mod_remesh.octree_depth = 8
            
            bpy.ops.object.modifier_apply(apply_as='DATA', modifier='mosh')
            bpy.ops.object.modifier_apply(apply_as='DATA', modifier='more')
            
            count = count + 1
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
            print("     Shrinkrap #%s finished" %(count))
        
        print("Shrinkwrap finished")
        
        ## Create Spongiosa ##
        '''
        Creates a grid if "Create Spongiosa-Grid" is checked with the Settings below. Will take time.
        '''
        
        if (scene.createGrid == True):
            
            tx = scene.diameterPillarX 
            ty = scene.diameterPillarY
            tz = scene.diameterPillarZ

            dx = scene.distancePillarX
            dy = scene.distancePillarY
            dz = scene.distancePillarZ

            (x,y,z) = objRec.dimensions

            verts = []
            faces = []

            xlen = 0
            ylen = 0
            zlen = 0

            ## create verts ##
            cx = 0
            cy = 0
            cz = 0

            while cz < z/(dz + tz) +1:
                
                # first two x-Rows
                zloc = cz*(dz + tz) - z/2 #zloc = location of first vert
                cy = 0
                while cy < y/(dy + ty) +1:
                    
                    # first x-Row
                    yloc = cy*(dy + ty) - y/2
                    cx = 0
                    while cx < x/(dx + tx) +1:
                        xloc = cx*(dx + tx) - x/2
                        
                        verts.append((xloc,yloc,zloc))
                        verts.append((xloc + tx, yloc, zloc))
                        
                        cx = cx + 1
                    
                    # second x-row y += ty
                    yloc = cy*(dy+ty) + ty - y/2
                    cx = 0
                    while cx < x/(dx + tx) +1:
                        xloc = cx*(dx + tx) - x/2
                        
                        verts.append((xloc,yloc,zloc))
                        verts.append((xloc + tx, yloc, zloc))
                        
                        cx = cx + 1
                    
                    cy = cy + 1
                    
                # second two x-Rows z += tz
                
                zloc = cz*(dz + tz) + tz - z/2 #zloc = location of first vert
                cy = 0
                while cy < y/(dy + ty) +1:
                    
                    # first x-Row
                    yloc = cy*(dy + ty) - y/2
                    cx = 0
                    while cx < x/(dx + tx) +1:
                        xloc = cx*(dx + tx) - x/2
                        
                        verts.append((xloc,yloc,zloc))
                        verts.append((xloc + tx, yloc, zloc))
                        
                        cx = cx + 1
                    
                    # second x-row y += ty
                    yloc = cy*(dy+ty) + ty - y/2
                    cx = 0
                    while cx < x/(dx + tx) +1:
                        xloc = cx*(dx + tx) - x/2
                        
                        verts.append((xloc,yloc,zloc))
                        verts.append((xloc + tx, yloc, zloc))
                        
                        cx = cx + 1
                    
                    cy = cy + 1
                    
                cz = cz + 1

                
            ## create faces ##
            xlen = cx*2 # number of vertex in row
            ylen = cy*2
            zlen = cz*2
            xylen = xlen*ylen

            print(xlen)
            print(ylen)
            print(zlen)

            cz = 0
            while cz < zlen/2:
                cy = 0
                while cy <= (ylen/2) -1:
                    cx = 0
                    while cx <= (xlen/2)-1:
                        if cy <= (ylen/2) -2:
                            # xy-level, y-direction
                            vert1 = ((cx*2 + 1 + xlen) + cy*2*xlen) + cz*xylen*2
                            vert2 = ((cx*2 + xlen) + cy*2*xlen) + cz*xylen*2
                            vert3 = ((cx*2 + 2*xlen) + cy*2*xlen) + cz*xylen*2
                            vert4 = ((cx*2 + 1 + 2*xlen) + cy*2*xlen) + cz*xylen*2
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            vert2 = ((cx*2 + 1 + xlen) + cy*2*xlen) + (cz+1)*ylen*xlen*2 - xylen
                            vert1 = ((cx*2 + xlen) + cy*2*xlen) + (cz+1)*ylen*xlen*2 - xylen
                            vert4 = ((cx*2 + 2*xlen) + cy*2*xlen) + (cz+1)*ylen*xlen*2 - xylen
                            vert3 = ((cx*2 + 1 + 2*xlen) + cy*2*xlen) + (cz+1)*ylen*xlen*2 - xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            # yz-level, y-direction
                            vert2 = ((cx*2 + xlen) + cy*2*xlen) + cz*xylen*2
                            vert1 = ((cx*2 + xlen) + cy*2*xlen) + xlen + cz*xylen*2
                            vert4 = ((cx*2 + xlen) + cy*2*xlen) + xlen + cz*xylen*2 + xylen
                            vert3 = ((cx*2 + xlen) + cy*2*xlen) + cz*xylen*2 + xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            vert1 = ((cx*2 + 1 + xlen) + cy*2*xlen) + cz*xylen*2
                            vert2 = ((cx*2 + 1 + xlen) + cy*2*xlen) + xlen + cz*xylen*2
                            vert3 = ((cx*2 + 1 + xlen) + cy*2*xlen) + xlen + cz*xylen*2 + xylen
                            vert4 = ((cx*2 + 1 + xlen) + cy*2*xlen) + cz*xylen*2 + xylen
                            faces.append((vert1,vert2,vert3,vert4))
                        
                        if cx <= (xlen/2)-2:
                            # xy-level, x-direction
                            vert2 = ((cx*2 + 1) + cy*2*xlen) + cz*xylen*2
                            vert1 = ((cx*2 + 2) + cy*2*xlen) + cz*xylen*2
                            vert4 = ((cx*2 + 2 + xlen) + cy*2*xlen) + cz*xylen*2
                            vert3 = ((cx*2 + 1 + xlen) + cy*2*xlen) + cz*xylen*2
                            faces.append((vert1,vert2,vert3,vert4))

                            vert1 = ((cx*2 + 1) + cy*2*xlen) + (cz+1)*ylen*xlen*2 - xylen
                            vert2 = ((cx*2 + 2) + cy*2*xlen) + (cz+1)*ylen*xlen*2 - xylen
                            vert3 = ((cx*2 + 2 + xlen) + cy*2*xlen) + (cz+1)*ylen*xlen*2 -xylen
                            vert4 = ((cx*2 + 1 + xlen) + cy*2*xlen) + (cz+1)*ylen*xlen*2 -xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            # xz-level, x-direction
                            vert1 = ((cx*2 + 1) + cy*2*xlen) + cz*xylen*2
                            vert2 = ((cx*2 + 2) + cy*2*xlen) + cz*xylen*2
                            vert3 = ((cx*2 + 2) + cy*2*xlen) + (cz+1)*xylen*2 - xylen
                            vert4 = ((cx*2 + 1) + cy*2*xlen) + (cz+1)*xylen*2 - xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            vert2 = ((cx*2 + 1) + cy*2*xlen + xlen) + cz*xylen*2
                            vert1 = ((cx*2 + 2) + cy*2*xlen + xlen) + cz*xylen*2
                            vert4 = ((cx*2 + 2) + cy*2*xlen + xlen) + (cz+1)*xylen*2 - xylen
                            vert3 = ((cx*2 + 1) + cy*2*xlen + xlen) + (cz+1)*xylen*2 - xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                        if cz <= (zlen/2)-2:
                            #xz-level, z-direction
                            vert1 = (cx*2) + cy*2*xlen + (cz+1)*xylen*2 - xylen
                            vert2 = (cx*2 + 1) + cy*2*xlen + (cz+1)*xylen*2 - xylen
                            vert3 = (cx*2 + 1) + cy*2*xlen + (cz+1)*xylen*2
                            vert4 = (cx*2) + cy*2*xlen + (cz+1)*xylen*2
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            vert2 = (cx*2) + (cy+1)*2*xlen-xlen + (cz+1)*xylen*2 - xylen
                            vert1 = (cx*2 + 1) + (cy+1)*2*xlen-xlen + (cz+1)*xylen*2 - xylen
                            vert4 = (cx*2 + 1) + (cy+1)*2*xlen-xlen + (cz+1)*xylen*2
                            vert3 = (cx*2) + (cy+1)*2*xlen-xlen + (cz+1)*xylen*2
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            #yz-level, z-direction
                            vert2 = (cx*2) + cy*2*xlen + (cz+1)*xylen*2 - xylen
                            vert1 = (cx*2) + (cy+1)*2*xlen-xlen + (cz+1)*xylen*2 - xylen
                            vert4 = (cx*2) + (cy+1)*2*xlen-xlen + (cz+1)*xylen*2
                            vert3 = (cx*2) + cy*2*xlen + (cz+1)*xylen*2
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            vert1 = (cx*2+1) + cy*2*xlen + (cz+1)*xylen*2 - xylen
                            vert2 = (cx*2+1) + (cy+1)*2*xlen-xlen + (cz+1)*xylen*2 - xylen
                            vert3 = (cx*2+1) + (cy+1)*2*xlen-xlen + (cz+1)*xylen*2
                            vert4 = (cx*2+1) + cy*2*xlen + (cz+1)*xylen*2
                            faces.append((vert1,vert2,vert3,vert4))
                        
                        # fill holes outside
                        if cz == 0:
                            # xy-level
                            vert2 = ((cx*2) + cy*2*xlen)
                            vert1 = ((cx*2 + 1) + cy*2*xlen)
                            vert4 = ((cx*2 + 1 + xlen) + cy*2*xlen)
                            vert3 = ((cx*2 + xlen) + cy*2*xlen)
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            vert1 = ((cx*2) + cy*2*xlen) + zlen*ylen*xlen - xylen
                            vert2 = ((cx*2 + 1) + cy*2*xlen) + zlen*ylen*xlen - xylen
                            vert3 = ((cx*2 + 1 + xlen) + cy*2*xlen) + zlen*ylen*xlen -xylen
                            vert4 = ((cx*2 + xlen) + cy*2*xlen) + zlen*ylen*xlen -xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                        if cy == 0:
                            # xz-level
                            vert1 = (cx*2) + cz*xylen*2
                            vert2 = (cx*2 + 1) + cz*xylen*2
                            vert3 = (cx*2 + 1) + (cz+1)*xylen*2 - xylen
                            vert4 = (cx*2) + (cz+1)*xylen*2 - xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            vert2 = ((cx*2) + (ylen-1)*xlen) + cz*xylen*2
                            vert1 = ((cx*2 + 1) + (ylen-1)*xlen) + cz*xylen*2
                            vert4 = ((cx*2 + 1) + (ylen-1)*xlen) + (cz+1)*xylen*2 - xylen
                            vert3 = ((cx*2) + (ylen-1)*xlen) + (cz+1)*xylen*2 - xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                        if cx == 0:
                            
                            vert2 = cy*2*xlen + cz*xylen*2
                            vert1 = (cy+1)*2*xlen-xlen + cz*xylen*2
                            vert4 = (cy+1)*2*xlen-xlen + (cz+1)*xylen*2-xylen
                            vert3 = cy*2*xlen + (cz+1)*xylen*2-xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                            vert1 = xlen-1 + cy*2*xlen + cz*xylen*2
                            vert2 = xlen-1 + (cy+1)*2*xlen-xlen + cz*xylen*2
                            vert3 = xlen-1 + (cy+1)*2*xlen-xlen + (cz+1)*xylen*2-xylen
                            vert4 = xlen-1 + cy*2*xlen + (cz+1)*xylen*2-xylen
                            faces.append((vert1,vert2,vert3,vert4))
                            
                        cx = cx + 1
                    cy = cy + 1
                cz = cz + 1

            
            ## build mesh ##
            mesh_data = bpy.data.meshes.new("Spongiosa")  
            mesh_data.from_pydata(verts, [], faces)  
            mesh_data.update()

            objSpon = bpy.data.objects.new("Spongiosa", mesh_data)

            objSpon.location = objRec.location  

            scene = bpy.context.scene    
            scene.objects.link(objSpon)
            
            objSpon.location = objRec.location
            bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
            print("     Grid created")    
            
            booleanMod(2, objSpon, objRec) 
            
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
            print("Spongiosa created")
        
             
        ## Delete by Distance ##
        '''
        Delets every face of "objRec" which has a larger distance to "objCT" than set in "Fracture Detail".
        '''
        
        # Get the active mesh
        scene.objects.active = objRec #bpy.data.objects["Sphere"]
        bpy.ops.object.mode_set(mode='EDIT')

        L = scene.fractureDetail # distance limit
        ct = objCT #bpy.data.objects.get(k_segmentname)
        mesh_obj = bpy.context.edit_object
        me = mesh_obj.data
        
        # Get a BMesh representation
        bm = bmesh.from_edit_mesh(me)
        for face in bm.faces:
            face.select = False
        
            v1 = mesh_obj.matrix_world * face.calc_center_median() # global face median
            localPos = ct.matrix_world.inverted() * v1  # face cent in sphere local space
        
            (res, loc, norm, face_index) = ct.closest_point_on_mesh(localPos)
        
            v2 = ct.matrix_world * loc
        
            if (v2 - v1).length > L:
                face.select = True
                
        bpy.ops.mesh.delete(type='FACE')
        bpy.ops.object.mode_set(mode='OBJECT')
        
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        print("Fracture gaps deleted")
        
        ## Clarify Fracture Gap
        '''
        Illiterates through all faces of "objCT" if "Clarify Fracture Gaps" is checked.
        If this Face is between two gaps, this face will be deleted.
        "Precision" means the maximal rad angle between these two gaps and the face.
        "Max. distance between" means the distance between the two gaps.
        '''
        
        if (scene.clarifyFracture == True):
            
            radius = scene.distanceGaps
            max_angle = scene.radiusGaps
            
            scene.objects.active = objRec
            bpy.ops.object.mode_set(mode='EDIT')
            mesh_obj = bpy.context.edit_object
            me = mesh_obj.data
            bm = bmesh.from_edit_mesh(me)

            bridge_list = list()
            edge_list = list()


            bpy.ops.mesh.select_all( action = 'DESELECT' )
            bpy.ops.mesh.select_mode( type = 'EDGE' )
            bpy.ops.mesh.select_non_manifold()

            vert_list = list()
                
            for vert in bm.verts:
                if vert.select == True:
                    vert_list.append(vert)

            bpy.ops.mesh.select_all( action = 'DESELECT' )
            faces_len = len(bm.faces)
            for face in bm.faces:
                print("     Face: %s" % (faces_len - face.index))
                if (face.index / 1000).is_integer():
                    bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
                
                # Select Holes #

                fCo = face.calc_center_median()
                verts = list()
                for v in vert_list:
                    
                    if (fCo - objRec.matrix_world * v.co ).length < radius:
                        verts.append(v)
                        #edge.select = True
                        
                
                if len(verts) > 1:
                    
                    # Angle #
                        
                    count = 0
                    while (len(verts) - count) > 0:
                        vec_ref = verts[count].co - face.calc_center_median()
                        count = count + 1
                        
                        c = count
                        while (len(verts) - c) > 0:
                            vec = verts[c].co - face.calc_center_median()
                            
                            ang = vec_ref.angle(vec)
                            
                            if ang > max_angle:
                                bridge_list.append(face)
                                count = len(verts)
                                print("     --- Bridge #%s added ---" % (len(bridge_list)))
                                face.select = True
                                break
                            
                            c = c + 1
                
            bpy.ops.mesh.select_all( action = 'DESELECT' )
            bpy.ops.mesh.select_mode( type = 'FACE' )
            for face in bridge_list:
                face.select = True
            
            bpy.ops.mesh.delete(type='FACE')
            bpy.ops.object.mode_set(mode='OBJECT')
            
            bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
            print("Fracture gaps clarified")
        
        
        ## Solidify ##
        '''
        Applies the solidify modifier to "objRec"
        '''
        
        # Get the active mesh
        scene.objects.active = objRec #bpy.data.objects["Sphere"]
        
        # Add Solidify Modifier with Thickness = 2.0
        bpy.ops.object.modifier_add(type='SOLIDIFY')
        bpy.context.object.modifiers["Solidify"].thickness = scene.thicknessCorticalis
        bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Solidify")
        
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        print("Solidified")
        
        ## Hide & Join ##
        '''
        Hides "objCT".
        If the grid above was created, it will be joined with "objRec".
        '''
        
        objCT.hide = True
        
        if (scene.createGrid == True):
            joinObjects([objRec, objSpon])
        
        time_end = time.time()
        print("Time: %s s" %(time_end-time_start))
        
        return {'FINISHED'}

bpy.utils.register_class(RecreateCTOperator)

class InvertDeleteOperator(bpy.types.Operator):
    '''
    Delets all not selected object in Edit Mode.
    '''
    
    bl_idname = "ct.invertdelete"
    bl_label = "Delete not selected Parts"

    def execute(self, context):
        
        bpy.ops.mesh.select_all(action='INVERT')
        bpy.ops.mesh.delete(type='VERT')
        
        
        return {'FINISHED'}

bpy.utils.register_class(InvertDeleteOperator)


class OriginJoinOperator(bpy.types.Operator):
    '''
    Joins all objects in the scene and sets the origin to the center of mass.
    The object will be renamed to "Bone".
    '''
    
    bl_idname = "ct.originjoin"
    bl_label = "Set Origin and Define"

    def execute(self, context):
        scene = bpy.context.scene
        
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        
        # join all objects and define
        
        for ob in scene.objects:
            ob.hide = False
        if len(scene.objects) == 0:
            #TODO: Error Message
            print('no object')
            self.report({'ERROR'}, "No Object found. Please import or create a Object.")
            
        if len(scene.objects) == 1:
            objCT = scene.objects[0]
        else:
            objCT = joinObjects(scene.objects)
        
        objCT = scene.objects.active
        objCT.name = 'Bone'
        objCT.select = True
        
        print('Objects joined')
        
        bpy.ops.object.origin_set(type='ORIGIN_CENTER_OF_MASS')
        objCT.location = (0,0,0)
        bpy.ops.object.transform_apply(location=True, rotation=False, scale=False)
        
        return {'FINISHED'}

bpy.utils.register_class(OriginJoinOperator)



def register():
    bpy.utils.register_module(__name__)
    
def unregister():
    bpy.utils.unregister_module(__name__)
    
if __name__ == "__main__":
    register()
